import {Alert, Nav} from 'components'
import {motion, Variants} from 'framer-motion'
import Head from 'next/head'
import Link from 'next/link'
import {useRouter} from 'next/router'

const ContainerVariants: Variants = {
  start: {
    opacity: 0,
    transition: {staggerChildren: 0.2},
  },
  end: {
    opacity: 1,
    transition: {staggerChildren: 0.3},
  },

  exit: {
    y: '100%',
    opacity: 0,
    transition: {
      type: 'spring',
    },
  },
}

const ItemVariants: Variants = {
  start: {
    opacity: 0,
    scale: 1.2,
    y: 150,
    transition: {},
  },
  end: {
    opacity: 1,
    y: '0%',
    scale: 1,
    transition: {type: 'spring', stiffness: 70, damping: 12, duration: 0.5},
  },
}

export default function Home() {
  const router = useRouter()
  return (
    <>
      <Head>
        <title>Indrajit Sarkar.</title>
      </Head>
      <Nav />
      <motion.div
        exit='exit'
        variants={ContainerVariants}
        initial='start'
        animate='end'
        className='container flex flex-col items-center justify-center w-screen min-h-screen px-2 mx-auto md:max-w-2xl'>
        <motion.img
          variants={ItemVariants}
          className='mb-3 transition-shadow duration-500 rounded-full hover:ring-4 ring-white ring-opacity-25 h-28 w-28'
          src='/images/me.jpg'
          alt='Indrajit Sarkar'
        />
        <motion.p
          variants={ItemVariants}
          className='mb-2 text-lg font-medium text-white md:text-2xl text-opacity-90'>
          👋 Hi, I'm Indrajit
        </motion.p>
        <motion.h1
          variants={ItemVariants}
          className='mb-5 text-3xl font-bold leading-tight tracking-normal text-center opacity-100 select-text md:text-5xl'>
          Designing, Building modern &amp; beautiful applications, websites.
        </motion.h1>
        <motion.p
          variants={ItemVariants}
          className='max-w-screen-sm text-lg text-center opacity-50 md:w-10/12'>
          I love designing and developing modern looking web and desktop
          application. I have a deep desire to excel and continuously improve in
          my work. Learn more about{' '}
          <Link href='/about'>
            <span className='font-semibold underline cursor-pointer text-primary'>
              me.
            </span>
          </Link>
        </motion.p>
        <Link href='/contact'>
          <motion.button
            variants={ItemVariants}
            className='mt-5 btn-white-outline'>
            Connect with me
          </motion.button>
        </Link>
      </motion.div>
      <Alert
        message='Download resume'
        showIcon
        action={() => router.push('/resume.pdf')}
      />
    </>
  )
}
