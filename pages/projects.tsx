import {
  FeaturedProjectCard as FeaturedProjectCardType,
  Nav,
  OtherProjectCard as OtherProjectCardType,
} from 'components'
import {
  ProjectContainerVariants,
  ProjectItemVariants,
} from 'components/Project/animation'
import {motion} from 'framer-motion'
import dynamic from 'next/dynamic'
import Head from 'next/head'
import Link from 'next/link'
import useSWR from 'swr'
import {fetcherFn} from 'swr/dist/types'
import {ProjectType} from 'utils/project-data'

const FeaturedProjectCard = dynamic(
  () => import('components').then(mod => mod.FeaturedProjectCard as any),
  {loading: () => <h1>Loading Featured Projects</h1>}
) as typeof FeaturedProjectCardType

const OtherProjectCard = dynamic(
  () => import('components').then(mod => mod.OtherProjectCard as any),
  {loading: () => <h1>Loading Other Projects</h1>}
) as typeof OtherProjectCardType

const fetcher: fetcherFn<ProjectType[]> = url =>
  fetch(url).then(res => res.json())

const projects = () => {
  const {data, error} = useSWR<ProjectType[]>('/api/projects', fetcher)
  const featuredProjects = data?.filter(item => item.featured && item)
  const otherProjects = data?.filter(item => !item.featured && item)

  if (error) {
    return (
      <motion.div
        exit='exit'
        variants={ProjectContainerVariants}
        initial='start'
        animate='end'
        className='flex items-center p-5 bg-primary text-dark'>
        <div className=''>
          <h1 className='text-2xl'>Something went wrong :(</h1>
          <p className='text-dark2'>Could not fetch project list.</p>
        </div>
        <Link href='/'>
          <button className='px-3 py-2 ml-auto text-white transition-all duration-500 rounded-full bg-primaryDark bg-opacity-60 hover:bg-opacity-80'>
            Home
          </button>
        </Link>
      </motion.div>
    )
  }

  if (!data) return <h1>Loading....</h1>

  return (
    <>
      <Head>
        <title>Projects | Indrajit Sarkar.</title>
      </Head>

      <Nav />

      <motion.div
        exit='exit'
        variants={ProjectContainerVariants}
        initial='start'
        animate='end'
        className='container py-5 mx-auto'>
        <motion.h1
          initial={{y: 50, opacity: 0}}
          animate={{
            y: 0,
            opacity: 1,
            transition: {
              duration: 0.5,
            },
          }}
          className='my-20 text-5xl font-semibold tracking-wider text-center text-white uppercase sm:text-7xl md:text-8xl lg:text-9xl md:mx-0'
          style={{
            WebkitTextFillColor: 'transparent',
            WebkitTextStroke: '2px rgba(255, 255, 255, 0.671)',
          }}>
          Projects
        </motion.h1>

        <motion.section
          variants={ProjectItemVariants}
          className='w-full px-5 mx-auto md:w-11/12 md:px-0'
          id='featured-projects'>
          {featuredProjects?.map((project, index) => (
            <FeaturedProjectCard
              key={project.name}
              project={project}
              imageOnLeft={index % 2 !== 0}
            />
          ))}
        </motion.section>

        <motion.section variants={ProjectItemVariants}>
          <h2 className='mt-24 font-semibold tracking-wider text-center uppercase text-primary'>
            Other Projects
          </h2>
          <div className='flex flex-wrap w-full max-w-screen-xl px-5 mx-auto mt-5 md:w-11/12 md:px-0'>
            {otherProjects?.map(project => (
              <OtherProjectCard key={project.name} project={project} />
            ))}
          </div>
          <Link href='#'>
            <button className='mx-auto mt-5 btn-white-outline'>
              Show more
            </button>
          </Link>
        </motion.section>
      </motion.div>
    </>
  )
}

export default projects
