import {
  useEffect as _useEffect,
  useLayoutEffect as _useLayoutEffect,
} from 'react'

const useEffect = typeof window !== 'undefined' ? _useLayoutEffect : _useEffect

export const useScrollLock = (isNavOpen: boolean) => {
  useEffect((): (() => void) => {
    const originalStyle: string = window.getComputedStyle(document.body)
      .overflow
    if (isNavOpen) document.body.style.overflow = 'hidden'
    return () => (document.body.style.overflow = originalStyle)
  }, [isNavOpen])
}
