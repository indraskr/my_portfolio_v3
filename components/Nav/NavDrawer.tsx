import {UiContext} from 'context/ui'
import {motion, Variants} from 'framer-motion'
import Link from 'next/link'
import {useRouter} from 'next/router'
import {useContext, useRef} from 'react'
import {AiFillGithub, AiFillLinkedin, AiOutlineInstagram} from 'react-icons/ai'
import {HiDocumentDownload} from 'react-icons/hi'
import {IconType} from 'react-icons/lib'
import {useOnClickOutside, useScrollLock} from 'utils/hooks'

const ContainerVariants: Variants = {
  show: {
    x: 0,
    opacity: 1,
    transition: {
      type: 'spring',
      stiffness: 50,
      staggerChildren: 0.2,
      when: 'beforeChildren',
    },
  },
  hide: {
    opacity: 0,
    x: '90%',
    transition: {stiffness: 40, staggerChildren: 0.4},
  },
}

const ItemVariants: Variants = {
  hide: {
    opacity: 0,
    x: '150%',
    y: 10,
    transition: {},
  },
  show: {
    opacity: 1,
    x: 0,
    y: 0,
    transition: {type: 'spring', stiffness: 40, duration: 1},
  },
  hover: {scale: 1.2},
  tap: {scale: 0.8},
}

const NavRoutes = [
  {title: 'Home', link: '/'},
  {title: 'About', link: '/about'},
  {title: 'Projects', link: '/projects'},
  {title: 'Blog', link: '/blog'},
  {title: 'Contact', link: '/contact'},
]

const NavDrawer = () => {
  const {isNavOpen, setIsNavOpen} = useContext(UiContext)
  const router = useRouter()
  const ref = useRef<HTMLDivElement | null>(null)

  useScrollLock(isNavOpen)
  useOnClickOutside(ref, () => setIsNavOpen(false))

  const NavLink = ({title, link}: {title: string; link: string}) => (
    <motion.h1
      animate={isNavOpen ? 'show' : 'hide'}
      variants={ItemVariants}
      whileTap='tap'
      whileHover='hover'
      className='cursor-pointer my-14'>
      <Link href={link}>{title}</Link>
    </motion.h1>
  )

  const NavIcon = ({link, icon: Icon}: {link: string; icon: IconType}) => (
    <motion.a
      whileHover={{translateY: -10, scale: 1.2}}
      href={link}
      target='_blank'
      rel='noopener noreferrer'>
      <Icon size='35' color='#3E56A8' />
    </motion.a>
  )

  return (
    <motion.div
      onPanStart={() => setIsNavOpen(!isNavOpen)}
      initial={false}
      ref={ref}
      animate={isNavOpen ? 'show' : 'hide'}
      variants={ContainerVariants}
      id='nav-drawar'
      className='fixed top-0 right-0 z-10 w-full bg-primary sm:w-2/4 md:w-1/4'>
      <div className='flex flex-col items-center justify-center min-h-screen'>
        <div
          className='absolute cursor-pointer top-2 right-2 md:left-2 max-w-min'
          onClick={() => {
            setIsNavOpen(!isNavOpen)
          }}>
          <svg
            width='20'
            height='20'
            viewBox='0 0 21 37'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'>
            <path
              d='M6.30315e-08 31.7143L13.125 18.5L3.78189e-07 5.28572L2.625 3.13028e-08L21 18.5L2.625 37L6.30315e-08 31.7143Z'
              fill='#3E56A8'
              fillOpacity='0.77'
            />
          </svg>
        </div>

        <div
          className='text-5xl font-semibold text-center select-none text-primaryDark'
          onClick={() => setIsNavOpen(!isNavOpen)}>
          {NavRoutes.map(
            ({link, title}) =>
              router.pathname !== link && <NavLink title={title} link={link} />
          )}
        </div>

        <motion.div
          animate={isNavOpen ? 'show' : 'hide'}
          variants={ItemVariants}
          className='absolute flex flex-row justify-around w-1/2 bottom-5'>
          <NavIcon link='https://github.com/dni9' icon={AiFillGithub} />
          <NavIcon
            link='https://linkedin.com/in/indraskr'
            icon={AiFillLinkedin}
          />
          <NavIcon link='#' icon={AiOutlineInstagram} />
          <NavIcon link='/resume.pdf' icon={HiDocumentDownload} />
        </motion.div>
      </div>
    </motion.div>
  )
}

export default NavDrawer
